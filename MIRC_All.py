﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import math
import io
import psycopg2
import bottle
import bottle_pgsql
import os,csv
import nltk
import pickle
import json
from bottle import Bottle, request, response, template, redirect
import numpy as np
import urllib.parse
import urllib.request as urllib2
from nltk.stem.wordnet import WordNetLemmatizer
from scipy.sparse import csr_matrix
import datetime
import re
import matplotlib.pyplot as plt
import pandas as pd
import uuid
from functions_for_database_CBIR.CBIR_functions_for_py3_opencv3 import *



__author__  = "Caroline"
__status__  = "production"
__version__ = "1.0.0"
__date__    = "Jan 14 2018"

lmtzr = WordNetLemmatizer()
dir = os.getcwd()
os.chdir(dir)
import pandas as pd
from bottle import route, run, template, static_file,request,redirect 

bottle.debug(True)
app = bottle.Bottle()
plugin = bottle_pgsql.Plugin('dbname=carolinecao user=postgres password=abc123!')
app.install(plugin)

# info={'searchresult_errorMsg':'','tfid':'','title':'','datasource':'','author':'','findings':'','imagepath':'','bestMatch':'','topNMatchs':[],'totalpagenumber':0,'search_text':'','current_page':1}

def getVectorOrMatrix(filename):
    with open(filename, "rb") as f:
        vectorOrMatrix=pickle.load(f)
    return vectorOrMatrix


tfTerm_MatrixFileName="ddx_Matrix/tfTermMatrix.data"
dotProductMatrixFileName="ddx_Matrix/matrix_A_v2.data"
matrix_ADFileName="ddx_Matrix/matrix_AD_v2.data"
tfidVectorFileName="ddx_Matrix/tfidVector.data"
alltermVectorFileName="ddx_Matrix/allterm_vector.data"
RadlexVectorFileName="ddx_Matrix/radlexVector.data"

print("Loading martix data...")
tfTerm_Matrix=getVectorOrMatrix(tfTerm_MatrixFileName)
dotProductMatrix=getVectorOrMatrix(dotProductMatrixFileName)
allterm_vector=getVectorOrMatrix(alltermVectorFileName)
tfid_vector=getVectorOrMatrix(tfidVectorFileName)
AD_Matrix=getVectorOrMatrix(matrix_ADFileName)
radlexVector=getVectorOrMatrix(RadlexVectorFileName)
A = csr_matrix(dotProductMatrix)
AD=csr_matrix(AD_Matrix)
T=csr_matrix(tfTerm_Matrix)

print("done with loading")

@app.route('/')
def searchmainpage():
    info={'searchresult_errorMsg':'','tfid':'','title':'','datasource':'','author':'','findings':'','imagepath':'','bestMatch':'','topNMatchs':[],'totalpagenumber':0,'search_text':'','current_page':1}
    return template('home', info)
@app.route('/detailpage')
def detailPage(db):
    info={}
    pageid = request.query.get('id')
    if not pageid:
        redirect("/")
    db.execute("SELECT datasource, title, authors, dateofcreation, history, findings, diagnosis, discussion, ddx, tref FROM tfinfo WHERE tfid ='" + pageid + "'")
    tfDic=db.fetchone()

    db.execute("select imagepath from imageinfo where tfid='" + pageid + "'")
    imagepathsRows = db.fetchall()
    imgPaths = []
    for row in imagepathsRows:
        imagepath = row['imagepath']
        imgPaths.append(imagepath.replace("/var/www/html/", "http://rasinsrv04.cstcis.cti.depaul.edu/"))

    info['infos'] = tfDic
    info['images'] = imgPaths
    info['searchresult_errorMsg']=''
    return template('detailPage',info)

# Ajax methods
@app.route('/login', method='POST')
def do_login(db):
    email=request.forms.get("loginEmail")
    if not email:
        email = ""
    password=request.forms.get("loginPassword")
    if not password:
        password = ""
    if check_login(str(email),db,str(password)):
        bottle.response.set_cookie("account_email", email)
        return {'success': 'true', 'message': 'Login successfully.','email':str(email)}
    else:
        return {'success': 'false', 'message': 'Login info is incorrect'}
def check_login(email,db,password):
    sql="select * from login where emailid='"+email+"'"
    db.execute(sql)
    login_info = db.fetchone()
     
    if login_info:
        password_db=login_info['password']  
        if str(password_db)==password:
            return True
        else:
            return False
    else:
        return False

@app.route('/feedback', method='POST')
def do_feedback(db):
    feedback=request.forms.get('feedback')
    feedback_email=request.forms.get('email')
    title=request.forms.get('title')
    tf_id=request.forms.get('tf_id')
    searchkey=request.forms.get('searchkey')
    matchsection=request.forms.get('matchsection')
    t_parameters=(str(feedback),str(feedback_email),str(title),str(tf_id),str(searchkey),str(matchsection))
    sql =  "insert into accmeasure (feedback, emailreviewer, title, tfid,searchkey,matchsection) values (%s,%s,%s,%s,%s,%s)"
    db.execute(sql,t_parameters)
    return {'success':'true'}


@app.route('/register', method='POST')
def do_register(db):
    first_name=request.forms.get('registerFirstName')
    if not first_name:
        first_name = ""
    last_name=request.forms.get('registerLastName')
    if not last_name:
        last_name = ""
    email=request.forms.get('registerEmail')
    if not email:
        email = ""
    password=request.forms.get('registerPassword')
    if not password:
        password = ""
    # designation="student" #TODO: Change for dropdown
    designation=request.forms.get('designation')
    if not designation:
        designation = "student"

    if check_register(str(email),db):
        t_parameters=(str(first_name),str(last_name),str(email),str(password),str(designation))
        sql =  "insert into login (fname, lname, emailid, password, designation) values (%s,%s,%s,%s,%s)"
        #return sql
        db.execute(sql,t_parameters)
        # return template('detailPage',info)
        response.set_cookie("account_email", email)
        return {'success': 'true', 'message': 'Regiester successfully.','email':str(email)}
    else:
        # print ("your account has already existed in the system")
        return {'success': 'false', 'message': 'your account has already existed in the system.'}

def check_register(email,db):
    sql="select * from login where emailid='"+email+"'"
    #t_parameters=(email,)
    db.execute(sql)
    login_info = db.fetchone()   
    if login_info:
        # print ("your account has already existed in the system")
        return False
        
    else:
        # print ("go ahead to register")
        return True




# @app.route('/register', method='POST')
# def do_register(db):
#     first_name=request.forms.get('registerFirstName')
#     return first_name
@app.route('/clearcookie')
def clearcookie():
    response.set_cookie('searchHistory', '', expires=0)
    return "{\"response\":\"done\"}"

@app.route('/otherresults_with_ids', method='GET')
def moreImageResults(db):
    otherTFids=request.query.get('moreResultIds')
    print(otherTFids)
    TFidArray=otherTFids.split('|')
    info={}
    if len(TFidArray)==0:
        info['searchresult_errorMsg']="no other image search results"
        return template('otherresults', info)
    else:
        queriedTFArray=fetchTFArray(TFidArray,db)
        info["moreresults"]=replaceUrlLink(db,queriedTFArray)
        return template('otherresults', info)


@app.route('/otherresults', method='GET')
def more_results(db):
    search_txt = request.query.get('search_text')
    search_txt = re.sub(r"\'", r"", search_txt)


    fetch_total_searching_number=200
    total_searching_number = 100 #best match and 3 other matchs
    sorted_result_vector = getSortedSearchResultVector(search_txt, A)

    results = SearchResultTeachingFile_forequals(search_txt, T, fetch_total_searching_number, sorted_result_vector)
    coocurrence_results = filterTFIDsFromVector(sorted_result_vector,fetch_total_searching_number)

    queriedTFArray = []
    info = {}
    if coocurrence_results == None and results == None:
        queriedTFArray = getDataBasedonQ_forTopN(search_txt,db,total_searching_number)

    else:
        addedResult=[x for x in coocurrence_results if x not in results]
        results += addedResult
        results = results[0:(total_searching_number)]
    if results is not None:
        print("else")

        queriedTFArray,queried_tfids=filterTFwithsametitles(fetchTFArray(results,db),fetch_total_searching_number)

        print("!!!!!!!!!!!!!!!!!!!!")
        for i in range(len(queriedTFArray)):
            print(queriedTFArray[i]["tfid"])
        print("!!!!!!!!!!!!!!!!!!!!")

    if queriedTFArray:
        replaceUrlLink(db,queriedTFArray)

        filtered_TFArrary=queriedTFArray[4:(total_searching_number)]
        info["moreresults"] = filtered_TFArrary
        return template('otherresults', info)
    else:
        info['searchresult_errorMsg']='We are sorry, no results found for '+search_txt+' at this time.'
        return template('results', info)




@app.route('/bothsearch',method='POST')
def both_search(db):
    alternative_number = 3 #4 alternative items
    best_search_number=4
    total_searching_number = 50
    fetch_alternative_number = 10 #get more alternative 
    threshold=0.12
    info = {}  
    info['searchresult_errorMsg']=''
    image_upload = request.files.get('imageupload')
    # search_txt = request.query.get('q').lower()
    search_txt = request.forms.get('search_txt').lower()
    search_txt = re.sub(r"\'", r"", search_txt)


    if image_upload is None:    
        # info['searchresult_errorMsg']='please upload image'
        # return template('home', info)
        if not search_txt:    
        # info={'searchresult_errorMsg':'please enter keywords'}
            info['searchresult_errorMsg']='please enter keywords or/and image'
            redirect("/")
        else:
            redirect("/mainsearch?page=1&q="+search_txt)
        return template('results', info)
    

    save_path = "./tmp_imageupload"
    if not os.path.exists(save_path):
        os.makedirs(save_path)
    file_path = "{path}/{file}".format(path=save_path, file=str(uuid.uuid1())+image_upload.filename)
    image_upload.save(file_path)
    query_image_arr = plt.imread(file_path)


    queriedTFArray,img_filtered_tfids = get_imagesearch_result(db, query_image_arr,threshold)

    print("========================search_txt================================")
    print(search_txt)
    print("========================search_txt================================")
    bestMatches_TFArray,best_match_tfids,results_all_textsearch_tfids = get_text_search_result(db,search_txt)

    print("===================all_textsearch_tfids===================")
    print(results_all_textsearch_tfids)

    intersaction_tfids=(np.intersect1d(img_filtered_tfids,results_all_textsearch_tfids)).tolist()
    print("==================image search ids===============")
    print(img_filtered_tfids)
    print("-------------------intersaction_tfids------------")
    print(intersaction_tfids)

    #delete the insteraction tf_ids from bestmatch
    imagefiltered_bestmatch_tfids=[x for x in results_all_textsearch_tfids if x not in intersaction_tfids]

    print("imagefiltered_bestmatch_tfids")
    print(imagefiltered_bestmatch_tfids)

    ordered_intersaction_tfids=order_intersaction(img_filtered_tfids, intersaction_tfids)
    ordered_intersaction_tfids.extend(imagefiltered_bestmatch_tfids)

    if ordered_intersaction_tfids is not None:
        queriedTFArray,intersaction_tfids=filterTFwithsametitles(fetchTFArray(ordered_intersaction_tfids,db),total_searching_number)

    print("intersaction_tfids")
    print(intersaction_tfids)

    alternative_result = SearchResultTeachingFile(search_txt,AD,fetch_alternative_number)
    alternative_TFArray_total = fetchTFArray(alternative_result,db)
    alternative_TFArray,alternative_tfids = filterTFwithsametitles(alternative_TFArray_total,alternative_number)

    ########################
    ########################
    ########################
    ########################
    ########################
    if queriedTFArray:
        replaceUrlLink(db,queriedTFArray)
        replaceUrlLink(db,alternative_TFArray)
        # addRelevantTerms(fetchRelevantterm(results, search_txt),queriedTFArray)
        addRelevantTerms(fetchRelevantterm(alternative_result, search_txt),alternative_TFArray)
        info["bestMatch"] = queriedTFArray[0]
        info["topNMatches"] = queriedTFArray[1:(best_search_number)]
        info["moreResultIds"] = '|'.join(intersaction_tfids[best_search_number:len(intersaction_tfids)])
        info["alternative"] =  alternative_TFArray

        print("moreResultIds")
        print(info["moreResultIds"])

    else:
        info["searchresult_errorMsg"]="no result from image search"
    info["search_text"]=""

    
    os.remove(file_path)
    return template('results', info)

def order_intersaction(imagesearch_tfids, intersaction_tfids):
    ordered_intersaction=[]
    for i in imagesearch_tfids:
        for j in intersaction_tfids:
            if int(j)==int(i):
                ordered_intersaction.append(int(i))
                continue

    print("==================imagesearch_tfids===============")
    print(imagesearch_tfids)
    print("==================intersaction_tfids===============")
    print(intersaction_tfids)
    print("==================ordered_intersaction===============")
    print(ordered_intersaction)
    
    return ordered_intersaction

def get_imagesearch_result(db, query_image_arr,threshold_):
    images_percent_for_kmeans = 0.08
    cluster_count = 50
    query_image_id = '10_5'
    save_files = False
    ellipse = False
    display_results = False
    image_return_count = 120 
    #threshold_ = 0.12 # what I chosen based on visual inspection of results
    
    cluster_centers = pd.read_csv('functions_for_database_CBIR/cluster_centers.csv', index_col=0)
    database_BoW_temp = pd.read_csv('functions_for_database_CBIR/database_BoW.csv')
    database_BoW_temp.index = database_BoW_temp.iloc[:,0]; del database_BoW_temp['Unnamed: 0']
    database_BoW_dict = database_BoW_temp.T.to_dict('list'); del database_BoW_temp
    
    query_SIFT_feats = sift(query_image_arr, ellipse=False)
    # PART H
    query_BoW_arr =  bag_of_words(query_SIFT_feats, cluster_centers, query=True)

    # PART I (using cosine distance NOT similarity)
    dist_dict = calc_dist_sim(query_BoW_arr, database_BoW_dict, method='cosine')

    # PART J (k is the number of images to return)
    closest_tfids = return_tfids(dist_dict, use_threshold=True, threshold=threshold_, k=image_return_count, distance=True)
    total_searching_number = 4 #best match and 3 other matchs
    #alternative_number = 3 #4 alternative items
    print("---image search: closest tfids-----")
    print(closest_tfids)
    print("---image search: closest tfids-----")
    queriedTFArray = []
    filtered_tfids = []
    if closest_tfids is not None:
        queriedTFArray,filtered_tfids=filterTFwithsametitles(fetchTFArray(closest_tfids,db),0)
        # fetchTFArray(closest_tfids,db)
    return queriedTFArray, filtered_tfids


@app.route('/imagesearch',method='POST')
def image_search(db):
    total_searching_number=4
    image_return_count = 20 # maximum number of images to be returned; might be lower due to threshold
    info = {}  
    info['searchresult_errorMsg']=''
    image_upload = request.files.get('imageupload')
    threshold=1
    if image_upload is None:    
        # info={'searchresult_errorMsg':'please enter keywords'}
        info['searchresult_errorMsg']='please upload image'
        return template('home', info)
    else:
        save_path = "./tmp_imageupload"
        if not os.path.exists(save_path):
            os.makedirs(save_path)
        file_path = "{path}/{file}".format(path=save_path, file=str(uuid.uuid1())+image_upload.filename)
        image_upload.save(file_path)
        #query_image_arr = plt.imread(file_path)
        query_image_arr = read_single_image(file_path)

        queriedTFArray,filtered_tfids = get_imagesearch_result(db, query_image_arr,threshold)

    if queriedTFArray:
        replaceUrlLink(db,queriedTFArray)

        info["bestMatch"] = queriedTFArray[0]
        info["topNMatches"] = queriedTFArray[1:(total_searching_number)]
        info["moreResultIds"] = '|'.join(filtered_tfids[(total_searching_number):len(filtered_tfids)])

    else:
        info["searchresult_errorMsg"]="no result from image search"
    info["search_text"]=""

    
    os.remove(file_path)
    return template('results', info)



def get_text_search_result(db, search_txt):

    total_searching_number = 4 #best match and 3 other matchs 
    alternative_number = 3 #4 alternative items
    fetch_best_match_number=100 # get more results in case duplicate
    fetch_alternative_number = 10 #get more alternative 

    sorted_result_vector = getSortedSearchResultVector(search_txt, A)

    # results = SearchResultTeachingFile(search_txt,T,total_searching_number)
    # coocurrence_results=SearchResultTeachingFile(search_txt,A,(total_searching_number))

    results = SearchResultTeachingFile_forequals(search_txt, T, fetch_best_match_number, sorted_result_vector)
    coocurrence_results = filterTFIDsFromVector(sorted_result_vector,fetch_best_match_number)


    queriedTFArray = []
    if coocurrence_results == None and results == None:
        queriedTFArray = getDataBasedonQ_forTopN(search_txt,db,fetch_best_match_number)
    else:
        addedResult=[x for x in coocurrence_results if x not in results]
        results += addedResult
        results = results[0:(fetch_best_match_number)]
    if results is not None:
        queriedTFArray=fetchTFArray(results,db)
    bestMatches_TFArray,best_match_tfids=filterTFwithsametitles(queriedTFArray,fetch_best_match_number)


    return bestMatches_TFArray, best_match_tfids,results


@app.route('/getdcmimage', method='GET')
def get_dcm_image(db):
    image_url=request.query.get('image_url')
    response = urllib2.urlopen(image_url)
    image = response.read()
    return image


@app.route('/mainsearch', method='GET')
def do_search(db):

    print("list")
    print(str(list))

    # response.set_header('Access-Control-Allow-Origin', '*')
    # response.set_header('Access-Control-Allow-Methods', 'GET')
    # response.set_header('Access-Control-Max-Age',21600)
    # response.set_header('Access-Control-Allow-Credentials','true')
    # response.set_header('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept, Authorization')

            #     h['Access-Control-Allow-Origin'] = origin
            # h['Access-Control-Allow-Methods'] = get_methods()
            # h['Access-Control-Max-Age'] = str(max_age)
            # h['Access-Control-Allow-Credentials'] = 'true'
            # h['Access-Control-Allow-Headers'] = \
            #     "Origin, X-Requested-With, Content-Type, Accept, Authorization"
            # if headers is not None:
            #     h['Access-Control-Allow-Headers'] = headers

    total_searching_number = 4 #best match and 3 other matchs 
    alternative_number = 3 #4 alternative items
    fetch_best_match_number=20 # get more results in case duplicate
    fetch_alternative_number = 10 #get more alternative 
    search_txt = request.query.get('q').lower()
    search_txt = re.sub(r"\'", r"", search_txt)

    search_history=request.get_cookie("searchHistory", secret='searchhistory-secret-key')

    #print (search_history)
    if search_history is not None and len(search_history)>0: 
        if search_txt is not search_history[-1]: 
            search_history.append(search_txt)
            response.set_cookie("searchHistory",search_history,secret='searchhistory-secret-key')
    else:
        search_history=[search_txt]
        response.set_cookie("searchHistory",search_history,secret='searchhistory-secret-key')

    #print ('result')    
    #print (search_history)

    info = {}
    info["search_history"]=search_history
    info["search_text"]=search_txt
    # info["page_number"] = int(request.query.get('page'))
    current_page=request.query.get('page')
    info['current_page']=current_page
    info['searchresult_errorMsg']=''
    if not search_txt:    
        # info={'searchresult_errorMsg':'please enter keywords'}
        info['searchresult_errorMsg']='please enter keywords'
        return template('results', info)
    else:
        # Get the top 6 results!
        #previous logic
        #queriedTFArray = getDataBasedonQ_forTopN(search_txt,db,6)
        #coocurrence logic

        ##############
        ###############
        #####

        print("========================search_txt================================")
        print(search_txt)
        print("========================search_txt================================")

        bestMatches_TFArray,best_match_tfids,bestmatch_results = get_text_search_result(db,search_txt)

        print("===================bestmatch_results===================")
        print(bestmatch_results)


        # Base on AD to get the alternative, and base on sorted_result_vector get the order if the weight is the same
        print("")
        print("")
        print("!!!!! MARTIX AD !!!!!!")
        alternative_result = SearchResultTeachingFile(search_txt,AD,fetch_alternative_number)

        alternative_TFArray_total = fetchTFArray(alternative_result,db)
        alternative_TFArray,alternative_tfids = filterTFwithsametitles(alternative_TFArray_total,alternative_number)

        # for item in alternative_TFArray_total:
        #     skip = False

        #     for alternative_item in alternative_TFArray:
        #         if alternative_item["title"] == item["title"]:
        #             skip = True
        #             break
        #     if not skip:
        #         alternative_TFArray.append(item)

        #     if len(alternative_TFArray) >= alternative_number:
        #         break

        # alternative_TFArray = alternative_TFArray[0:(alternative_number)]


        if bestMatches_TFArray:
            replaceUrlLink(db,bestMatches_TFArray)
            replaceUrlLink(db,alternative_TFArray)

            bestMatches_TFArray=addRelevantTerms(fetchRelevantterm(bestmatch_results, search_txt),bestMatches_TFArray)
            addRelevantTerms(fetchRelevantterm(alternative_result, search_txt),alternative_TFArray)
            print("------bestmatch results------")
            print (bestMatches_TFArray[0])

            info["bestMatch"] = bestMatches_TFArray[0]
            a = bestMatches_TFArray[1:(total_searching_number)]

            info["topNMatches"] = bestMatches_TFArray[1:(total_searching_number)]


            info["alternative"] =  alternative_TFArray
            return template('results', info)
        else:
            info['searchresult_errorMsg']='We are sorry, no results found for '+search_txt+' at this time.'
            return template('results', info)

def filterTFwithsametitles(TFArray_total,display_number):

    # print("!!!!filterTFwithsametitles")
    # for i in range(len(TFArray_total)):
    #     print(TFArray_total[i]["tfid"])
    # print("!!!!filterTFwithsametitles")
    TFArray = []
    tfids=[]
    for item in TFArray_total:
        skip = False
        for _item in TFArray:
            if _item["title"] == item["title"]:
                skip = True
                break
        if not skip:
            TFArray.append(item)
            tfids.append(item["tfid"])
        if display_number!=0:
            if len(TFArray) >= display_number:
                return TFArray,tfids
    return TFArray,tfids




def fetchRelevantterm(tfIDs, search_txt):
    if tfIDs is None or len(tfIDs) == 0:
        return []

    relevanttermArray = []
    for Result_tfid in tfIDs:
        position=tfid_vector.index(Result_tfid)
        #relevanttermsPositions=np.argsort(tfTerm_Matrix[position])[-4:]
        relevanttermsPositions=np.argsort(dotProductMatrix[position])[-4:]
        relevantTerm_eachtfid=[]
        for relevanttermposition in relevanttermsPositions:
            if allterm_vector[relevanttermposition]!=search_txt:
                relevantTerm_eachtfid.append(allterm_vector[relevanttermposition])
        relevanttermArray.append(relevantTerm_eachtfid)
    return relevanttermArray


def fetchTFArray(tfIDs,db):
    if tfIDs is None or len(tfIDs) == 0:
        return []

    sql = "select t.tfid,t.title,t.datasource,t.authors,t.findings,t.diagnosis from tfinfo t where t.tfid IN ("

    orderBy = ""
    index = 1
    for tfid in tfIDs:
        if index==1:
            sql+="'"+str(tfid)+"'"
        else:
            sql+=",'"+str(tfid)+"'"


        orderBy += "WHEN '" + str(tfid) + "' THEN " + str(index) + " "
        index += 1
    sql += ") ORDER BY CASE t.tfid " + orderBy + " END"
    print(sql)
    db.execute(sql)
    queriedTFArray = db.fetchall()
   
    return queriedTFArray


def addRelevantTerms(relevanttermArray,queriedTFArray):
    if relevanttermArray is None:
        return []
    if queriedTFArray is None:
        return []

    i=0
    for row in queriedTFArray:
        if i < len(relevanttermArray):
            row['relevant_term']=relevanttermArray[i]
        else:
            break

        print("-----Relevant Terms-------")
        print(relevanttermArray[i],row['tfid'])
        i+=1
    return queriedTFArray

def replaceUrlLink(db,queriedTFArray):
    # print(str(len(queriedTFArray)))
    # Replace Image Url links
    for index in range(len(queriedTFArray)):
        row = queriedTFArray[index]
        # print(str(row))
        #print(row)
        tfid=row['tfid']
        db.execute("select imagepath from imageinfo where tfid='"+tfid+"'")
        # queriedImagePath = db.fetchone()
        queriedImagePaths = db.fetchall()

        paths = []
        for path in queriedImagePaths:
            imagepath=path['imagepath']
            imagepath_updated=imagepath.replace("/var/www/html/", "http://rasinsrv04.cstcis.cti.depaul.edu/")
            paths.append(imagepath_updated)
        if len(paths)==0:
            print("no image path"+tfid)
            row['imagepaths'] = []
            row['imagepath'] = ""
        else:
            row['imagepaths'] = paths
            row['imagepath'] = paths[0]
            #row['imagepaths'] = ["https://raw.githubusercontent.com/ivmartel/dwv/master/tests/data/bbmri-53323851.dcm","https://raw.githubusercontent.com/ivmartel/dwv/master/tests/data/bbmri-53323851.dcm","https://raw.githubusercontent.com/ivmartel/dwv/master/tests/data/bbmri-53323851.dcm"]
            #row['imagepath']="https://raw.githubusercontent.com/ivmartel/dwv/master/tests/data/bbmri-53323851.dcm"

    return queriedTFArray



def processSearchtext(search_txt):
    #searchText=urllib.parse.unquote(search_txt.lower())
    synind = 1
    negind = 0
    search_list=[]

    if negind == 0:
        neg_df = antdict(search_txt)   
        if len(neg_df) > 0:
            for i in neg_df:
                separate_txt_array=i.split()
                if len(separate_txt_array)==1:
                    search_list.append(i)
                else:
                    for j in separate_txt_array:
                        search_list.append(j)               
    
    if len(search_list) == 0:
        termRe = re.compile( " |".join(allterm_vector))
        q=termRe.findall(search_txt+' ')
        search_list=[x.strip(' ') for x in q]
        # if search_txt not in radlexVector: 
        #     search_list=search_txt.split()
        #     #递归
        #     #loop 1 
        #     ##      renal artery good apple => renal artery good apple
        #     #loop 2
        #     ##      renal artery good apple => renal artery good, artery good apple
        #     #loop 3
        #     ##      renal artery good apple => renal artery, artery good, good apple
        #     #loop 4
        #     ##      renal artery good apple => renal, artery, good, apple

        #     #loop 1 
        #     ##      apple => apple

        #     if len(search_list)>2:
        #         word_pairs = [search_list[i]+' '+search_list[i+1] for i in range(len(search_list)-1)]
        #         for pair in word_pairs:
        #             if pair in radlexVector:
        #                 search_list.append(pair)
        #                 for word in pair.split():
        #                     search_list.remove(word)
        #     if len(search_list)>3:
        #         three_words = [search_list[i]+' '+search_list[i+1]+' '+search_list[i+2] for i in range(len(search_list)-2)]
        #         for three_word in three_words:
        #             if three_word in radlexVector:
        #                 search_list.append(three_word)
        #                 for word in pair.split():
        #                     search_list.remove(word)
        # else:
        #     search_list.append(search_txt)
    if synind == 1:
        syn_df = KeywordProcess(search_txt)
        if len(syn_df) > 0:
            for i in syn_df:
                search_list.append(i)
                
    print("-------!!!search_list!!!----------")
    print (search_list)
    print("-------!!!search_list!!!----------")
    return search_list


def generatesql(search_txt):
    synind = 1
    # cooind = 0
    negind = 1
    expandlst = []
    sql_query=[]
    sql_query.append("")# for count
    sql_query.append("")# for query
    params_t = []
    params_t.append('')
    params_t.append('')

    if negind == 1:
        neg_df = antdict(search_txt)   
        if len(neg_df) > 0:
            for i in neg_df:
                expandlst.append(i)               
    
    if len(expandlst) == 0:    
        
        if synind == 1:
            syn_df = KeywordProcess(search_txt)
            if len(syn_df) > 0:
                for i in syn_df:
                    expandlst.append(i)

        # if cooind == 1:
        #     coo_df = cosearch(search_txt)   
        #     if len(coo_df) > 0:
        #         for i in coo_df:
        #             expandlst.append(i)

    if len(expandlst) == 0:

        
        #sql_query[0]="select count(t.title) from tfinfo t where t.tfid IN (select tfid from indexalltext where lower(alltext) like ('%" + search_txt.lower() + "%'))"
        sql_query[0]="select count(t.title) from tfinfo t where t.tfid IN (select tfid from indexalltext where lower(alltext) like '%%s') "
        params_t[0] = ('%'+search_txt.lower()+'%')

        #sql_query[1]="select t,tfid,t.title,t.datasource,t.authors,t.findings from tfinfo t where t.tfid IN (select tfid from indexalltext where lower(alltext) like ('%"+search_txt.lower()+"%'))"
        sql_query[1]="select t,tfid,t.title,t.datasource,t.authors,t.findings from tfinfo t where t.tfid IN (select tfid from indexalltext where lower(alltext) like '%%s') "
        params_t[1] = ('%'+search_txt.lower()+'%')

    else:
        # when found expansion
        #inner_sqltext = "select tfid from indexalltext where lower(alltext) like '%" + search_txt.lower() + "%'"
        inner_sqltext = "select tfid from indexalltext where lower(alltext) like '%%s'"
        for wd in expandlst:
            #inner_sqltext += str(" or lower(alltext) like '%" + wd.strip() + "%'")
            inner_sqltext += str(" or lower(alltext) like '%%s'")
            #sql_query[0] is for pagination only
            sql_query[0]= "select count(t.title) from tfinfo t where t.tfid in ("+inner_sqltext+")"
            params_t[0] = ('%'+search_txt.lower()+'%', '%'+wd.strip()+'%')

            sql_query[1] = "select t.tfid,t.title,t.datasource,t.authors,t.findings from tfinfo t where t.tfid IN (" + inner_sqltext + ")"
            params_t[1] = ('%'+search_txt.lower()+'%', '%'+wd.strip()+'%')

    return sql_query, params_t



def getDataBasedonQ_forTopN(search_txt,db,topN):
    sql_query,sql_t=generatesql(search_txt)
    sql_topN=sql_query[1]+" limit "+str(topN)

    print(sql_topN)
    print(sql_t[1])


    db.execute(sql_topN, sql_t[1])
    queriedTFArray = db.fetchall()
    return queriedTFArray


def getDataBasedonQ(search_txt,db,current_page):
    #sql_query="select count(*) from tfinfo t where t.tfid IN (select tfid from indexalltext where lower(alltext) like ('%" + search_txt.lower() + "%')) "
    sql_query,sql_t=generatesql(search_txt)
    db.execute(sql_query[0],sql_t[0])
    countArray=db.fetchone()
    LimitPerPage = 32
    count=countArray['count']
    totalpagenumber=math.ceil(count/LimitPerPage)
    # info['totalpagenumber']=int(totalpagenumber)
    Offset=(int(current_page) - 1)*LimitPerPage
    # syn_df = pd.read_csv("Syn_Dict.csv")
    # if search_txt.lower() in syn_df["Term"].values:
    #     temp = syn_df[syn_df["Term"] == search_txt.lower()]
    #     if pd.isnull(temp["Synonym"].values):
    #         db.execute("select t.tfid, t.title,t.datasource,t.authors,t.findings from tfinfo t where t.tfid IN (select tfid from indexalltext where lower(alltext) like ('%" + search_txt.lower() + "%')) limit "+str(LimitPerPage)+"offset "+str(Offset))
    #     else:
    #         # when found synonym
    #         term_syn = unlist(temp["Synonym"].values).split(',')
    #         inner_sqltext = "select tfid from indexalltext where lower(alltext) like '%" + search_txt.lower() + "%'"
    #         for wd in term_syn:
    #             inner_sqltext += str(" or lower(alltext) like '%" + wd.strip() + "%'")
    #         sql_text = "select t.title,t.datasource,t.authors,t.findings from imageinfo i, tfinfo t where t.tfid IN (" + inner_sqltext + ")"+"limit "+str(LimitPerPage)+"offset "+str(Offset)
    #         print(sql_text)
    #         db.execute(sql_text)
    # else:
    sql_pagelimit=sql_query[1]+"limit "+str(LimitPerPage)+"offset "+str(Offset)
    db.execute(sql_pagelimit,sql_t[1])
    queriedTFArray = db.fetchall()
    return queriedTFArray,int(totalpagenumber)

def unlist(ls):
    output = ''
    for v in ls:
        output = output + " " + v
    return output.strip()

#module to find synonyms with Radlex text file
#searches for whole term first, then individual terms
def KeywordProcess(origq):
    # first argument - query string

    # opens and loads synonym text file

    f = io.open("Synonyms.txt", "r", encoding="ISO-8859-1")
    syn_df = f.read().split('\n')
    f.close()

    # sets up empty list - this will be returned at end of function

    syn_text = []

    # removes punctuation and sets query to lower, also splits into separate terms

    tbl = str.maketrans("-<>)(.!?,'*:;#+/\"\\&", "%%%%%%%%%%%%%%%%%%%")
    fullq = origq.translate(tbl).replace("%", "").lower()
    splitq = fullq.split()

    # main loop - looks through synonym file to find matches with the query

    for line in syn_df:

        # splits each line of the syn file

        x1 = line.translate(tbl)
        x2 = x1.replace("%", "").lower()
        keywords = x2.split('|')

        # checks for full term match in each line

        if len(splitq) > 1:

            for kw in keywords:
                if kw == fullq:
                    for syn in keywords:
                        if syn != fullq:
                            syn_text.append(syn)

        # checks for single term match in each line

        for kw in keywords:
            for word in splitq:
                if kw == word:
                    for syn in keywords:
                        if syn != word:
                            syn_text.append(syn)
    print ("!!!!--------syn_text------!!!!")
    print (syn_text)
    print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")

    return syn_text
# def pagination(curentpage,totalpage,keyword):
#
# module to find co occurrence terms in text file
# searches for each individual term

# def cosearch(qtext):
#     # first argument - query string

#     # opens and loads cooccur text file

#     f = open("outputcoolem.txt", "r", encoding="utf-8")
#     coo_df = f.read().split('\n')
#     f.close()

#     # sets up empty list - this will be returned at end of function

#     coo_text = []

#     # removes punctuation and sets query to lower, also splits into separate terms

#     tbl = str.maketrans("-<>)(.!?'*:;#+/\"\\&", "%%%%%%%%%%%%%%%%%%")
#     q1 = qtext.translate(tbl)
#     q2 = q1.replace("%", "").lower()
#     query = q2.split(' ')

#     # lemmetizes each term - the cooccurr file is already lemmetized
#     # if non lemmed words are needed comment out this piece and change loop to use 'query'

#     lquery = []
#     for qw in query:
#         lquery.append(lmtzr.lemmatize(qw))

#     # parameter for how man co occurrence terms to use - limit 10
#     n = 5

#     # main loop - looks through cooccurr file to find matches with the query terms

#     for line in coo_df:

#         # splits each line of the cooccurr file

#         x1 = line.translate(tbl)
#         x2 = x1.replace("%", "").lower()
#         keywords = x2.split(',')

#         # checks for match with first word in each line - if a match then appends n cooccurr terms

#         for word in lquery:
#             if keywords[0] == word:
#                 for j in keywords[1:n + 1]:
#                     coo_text.append(j)

#     return coo_text


def antdict(query):
    antonym_needed = False
    antword = ['no ', 'not ', 'without ']
    for word in antword:
        wordindex = query.find(word)
        if wordindex >= 0:
            antonym_needed = True
            search_antonym_text = query[(wordindex + len(word)):]
            negation_term = query[wordindex:]
            search_text = query[:wordindex]

    search_text = ' '.join([word for word in query.split()])

    keywords = []

    # Antonym list
    antonym_text = []
    ant_df = pd.read_csv("IRIS_Antonym.csv", encoding="ISO-8859-1")

    # Search for Antonym if needed
    if antonym_needed == True:
        # print(search_antonym_text)
        if search_antonym_text in ant_df["Term"].values:
            temp = ant_df[ant_df["Term"] == search_antonym_text]
            antonym_text = unlist(temp["Antonym"].values).split('|')
            antonym_text.append(negation_term)
        else:
            antonym_text = [negation_term]
        # print(len(antonym_text))
        if len(antonym_text) == 1:
            keywords.append(antonym_text[0])
        else:
            for r in antonym_text:
                if r != negation_term:
                    keywords.append(r)

    return keywords


@app.route('/mainsearch', method='POST')
def do_direct(db):
    # search_txt = request.forms.get('search_txt')
    # search_txt = "abc"
    search_txt = request.forms.get('search_txt')
    redirect("/mainsearch?page=1&q="+search_txt)



@app.route('/show',method='GET')
def show(db):
    db.execute('SELECT * from teachingfile limit 10')
    row = db.fetchall()
    # df = db.fetchall()
   
    if row:
        return template('{{row}}', row=row)
    return HTTPError(404, "Entity not found")



@app.route('/fetchAlternative',method='POST')
def fetchAlternative(db):
    return_result={'success':0,'tfids':[],'message':'no alternative results'}
    searchText=urllib.parse.unquote(request.forms.get('search').lower())

    results = SearchResultTeachingFile(searchText,A,3)
    # t=(results[0],results[1],results[2],)
    # db.execute('select t.tfid,t.title,t.datasource,t.authors,t.findings from tfinfo t where t.tfid IN (?,?,?)',t)

    # 2323 2322 2321
    if results is not None:
        sql = "select t.tfid,t.title,t.datasource,t.authors,t.findings from tfinfo t where t.tfid IN ('"+str(results[0])+"','"+str(results[1])+"','"+str(results[2])+"')"
        #print(sql)
        db.execute(sql)
        queriedTFArray = db.fetchall()
        updated_queriedTFArray=replaceUrlLink(db,queriedTFArray)
        return_result['success']=1
        return_result['tfids']=updated_queriedTFArray
        return_result['message']='success'

        return json.dumps(return_result)
    else:
        return json.dumps(return_result)


################################################
# Helping Methods                              #
# For Alternative Diagnosis or Considerations: #
################################################

# tfidVectorFileName="ddx_Matrix/tfidVector.data"
# dotProductMatrixFileName = "ddx_Matrix/dotProductMatrix.data"
# # tfTermMatrixFileName="ddx_Matrix/tfTermMatrix.data"
# RadlexVectorFileName="ddx_Matrix/radlexVector.data"





def generateQ(searchText):
    #search_text_vector=searchText.split()
    search_text_vector=processSearchtext(searchText)
    print("---!!!beforegenerateQ!!----")
    print(search_text_vector)
    print("---!!!beforegenerateQ!!----")
    Q=[]
    #radlex_vector=getradlex_vector()
    for i in range(len(allterm_vector)):
        if allterm_vector[i] in search_text_vector:
            Q.append(1)
        else:
            Q.append(0)
    return Q



def getSortedSearchResultVector(searchText,matrix):
    #print(":::::::::::getOrderedSearchResultVector - Matrix:::::::::::")
    #print(matrix)
    #print("===========getOrderedSearchResultVector - Matrix===========")
    Q=generateQ(searchText)
    queryVectorBeforeTranspose=np.array(Q)
    if(np.count_nonzero(queryVectorBeforeTranspose)>0):
        queryVector=queryVectorBeforeTranspose.T
        result=matrix.dot(queryVector)

        # print(":::::::::::getOrderedSearchResultVector - queryVector:::::::::::")
        # print(queryVector)
        # print("===========getOrderedSearchResultVector - queryVector===========")

        # print(":::::::::::getOrderedSearchResultVector - result:::::::::::")
        # print(result)
        # print("===========getOrderedSearchResultVector - result===========")

        sored_result = sorted(zip(result, tfid_vector), reverse=True)

        # print(":::::::::::getOrderedSearchResultVector - sored result:::::::::::")
        # print(sored_result)
        # print("===========getOrderedSearchResultVector - sored result===========")

        return sored_result
    else:
        return None
    # print (SearchResultVector)


def filterTFIDsFromVector(vector,n):
    if vector is not None:
        HighestWeightedTFfiles = vector[:n]
        tfIds = []
        for i in range(len(HighestWeightedTFfiles)):
            if HighestWeightedTFfiles[i][0]!=0 and not math.isnan(HighestWeightedTFfiles[i][0]):
                tfIds.append(HighestWeightedTFfiles[i][1])
            #todo: if less than 6, add some coocurrence ones
        return tfIds

    return None;

def SearchResultTeachingFile(searchText,matrix,n):
    SearchResultVector=getSortedSearchResultVector(searchText,matrix)
    return filterTFIDsFromVector(SearchResultVector, n)


def order_for_same_weight(same_weight_tfids, co_score):
    if len(same_weight_tfids) > 1:
        print("same weight happen")
        temp_order_list = []  # [[weight:tfid]]
        # getting the order list from co_score
        for x in range(len(same_weight_tfids)):
            for y in range(len(co_score)):
                if same_weight_tfids[x] == co_score[y][1]:
                    temp_order_list.append(co_score[y])

        # sort by weight from height to low
        temp_order_list = sorted(temp_order_list, reverse=True)

        # put the sorted tfids back to same_weight_tfids
        return [x[1] for x in temp_order_list]
    return same_weight_tfids

def SearchResultTeachingFile_forequals(searchText,matrix,n,co_score):
    # print("::::::::::::::::::: SearchResultTeachingFile_forequals - Matrix ::::::::::::::::::")
    # print(matrix)
    # print("=================== SearchResultTeachingFile_forequals - Matrix ===================")

    SearchResultVector=getSortedSearchResultVector(searchText,matrix)
    # print("::::::::::::::::::: SearchResultTeachingFile_forequals - SearchResultVector ::::::::::::::::::")
    # print(SearchResultVector)
    # print("=================== SearchResultTeachingFile_forequals - SearchResultVector ===================")

    if SearchResultVector is not None and len(SearchResultVector) > 0:
        #tfid_vector=getTfidVector()
        HighestWeightedTFfiles=SearchResultVector[:n]

        tfIds = []

        last_weight = HighestWeightedTFfiles[0][0]
        same_weight_tfids = []

        print("Number of HighestWeightedTFfiles: " + str(len(HighestWeightedTFfiles)))

        for i in range(len(HighestWeightedTFfiles)):
            if HighestWeightedTFfiles[i][0]!=0 and not math.isnan(HighestWeightedTFfiles[i][0]):

                # If the weight is different, put same_weight_tfids to tdids
                if HighestWeightedTFfiles[i][0] != last_weight:

                    print("same_weight_tfids with " + str(i) +":")
                    print(same_weight_tfids)

                    # if there is more then 1 same_weight_tfids, do the order base on co_score
                    tfIds.extend(order_for_same_weight(same_weight_tfids, co_score))
                    same_weight_tfids.clear()

                    print(tfIds)

                # put all the same weight tf id to the buffer array same_weight_tfids
                same_weight_tfids.append(HighestWeightedTFfiles[i][1])
                last_weight = HighestWeightedTFfiles[i][0]

        tfIds.extend(order_for_same_weight(same_weight_tfids, co_score))

        #todo: if less than 6, add some coocurrence ones

        # print("::::::::::::::::::: SearchResultTeachingFile_forequals - tfIds ::::::::::::::::::")
        # print(tfIds)
        # print("=================== SearchResultTeachingFile_forequals - tfIds ===================")

        return tfIds
    else:
        return None
    

@app.route('/about',method='GET')
def showAbout(db):
    return template('about', {})

@app.route('/service',method='GET')
def showAbout(db):
    return template('service', {})

@app.route('/contact',method='GET')
def showAbout(db):
    return template('contact', {})

@app.route('/static/<dir>/<filename>')
def server_static(dir,filename):
    return static_file(filename, root='./static/'+dir+"/")


app.run(host='localhost', port=8087, debug=True)
