function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function Evaluation (feedback,tfid,title,search_text,div_id,matchsection,element) {


	var feedback_email=$('#email_login').text();
    if (feedback_email=="" ||feedback_email==null){
        alert("please log in first")
    }
    else{
        $.ajax({
                url: '/feedback',
                data: {'feedback': feedback, 'email': feedback_email,'title':title,'tf_id':tfid,'searchkey':search_text,'matchsection':matchsection},
                type: 'POST',
            }).done(function(data) {
                $(element).find('button').addClass('selected-btn');

                id='#'+div_id
                html="<p class='thanks'>Thanks for your feedback</p>"
                $(id).replaceWith(html);
            });
    }

    return false;
}

function cleanCookies() {
    $.ajax({
            url: '/clearcookie',
            type: 'GET',
        }).done(function(data) {
            $("#history_cookies").html("");
        });
}