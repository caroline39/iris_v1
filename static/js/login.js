   
$(function() {
    
    var $formLogin = $('#login-form');
    var $formLost = $('#lost-form');
    var $formRegister = $('#register-form');
    var $divForms = $('#div-forms');
    var $modalAnimateTime = 300;
    var $msgAnimateTime = 150;
    var $msgShowTime = 2000;
    var $rg_username=""

    $( document ).ready(function() {
    var email = Cookies.get('account_email');
    if (email != null) {
        html="<a id='email_login'>"+email+"</a>"
                    $('#login_botton').replaceWith(html);
    }
    });
    $("#login-form").submit(function () {
        // var $lg_username=$('#login_firstname').val();
        // var $lg_password=$('#login_password').val();
        // if ($lg_username == "ERROR") {
        //     msgChange($('#div-login-msg'), $('#icon-login-msg'), $('#text-login-msg'), "error", "glyphicon-remove", "Login error");
        // } else {
        //     msgChange($('#div-login-msg'), $('#icon-login-msg'), $('#text-login-msg'), "success", "glyphicon-ok", "Login OK");
        // }
        $.ajax({
                url: '/login',
                data: $('form').serialize(),
                type: 'POST',
            }).done(function(data) {
                if (data['success'] == 'true') {
                    msgChange($('#div-login-msg'), $('#icon-login-msg'), $('#text-login-msg'), "success", "glyphicon-ok", data['message']);
                    setTimeout(function(){
                        $('#login-modal').delay(8000).modal('hide');
                    }, 2000);
                    success_email_address=data['email']
                    html="<a id='email_login'>"+success_email_address+"</a>"
                    $('#login_botton').replaceWith(html);
                } else {
                    msgChange($('#div-login-msg'), $('#icon-login-msg'), $('#text-login-msg'), "error", "glyphicon-remove", data['message']);
                }
            });
            return false;
        });
    $("#lost-form").submit(function () {
        var $ls_email=$('#lost_email').val();
        if ($ls_email == "ERROR") {
            msgChange($('#div-lost-msg'), $('#icon-lost-msg'), $('#text-lost-msg'), "error", "glyphicon-remove", "Send error");
        } else {
            msgChange($('#div-lost-msg'), $('#icon-lost-msg'), $('#text-lost-msg'), "success", "glyphicon-ok", "Send OK");
        }
        
        return false;
    });

    $("#register-form").submit(function () {
        $.ajax({
            url: '/register',
            data: $('form').serialize(),
            type: 'POST',
        }).done(function(data) {
                // var $rg_firstname=$('#register_firstname').val();
                // var $rg_email=$('#register_email').val();
                // var $rg_password=$('#register_password').val();
                
                if (data['success'] == 'true') {
                    msgChange($('#div-register-msg'), $('#icon-register-msg'), $('#text-register-msg'), "success", "glyphicon-ok", data['message']);
                    setTimeout(function(){
                        $('#login-modal').delay(8000).modal('hide');
                    }, 2000);
                    success_email_address=data['email']
                    html="<a id='email_login'>"+success_email_address+"</a>"
                    $('#login_botton').replaceWith(html);
                } else {
                    msgChange($('#div-register-msg'), $('#icon-register-msg'), $('#text-register-msg'), "error", "glyphicon-remove", data['message']);
                }
          
            // $('#login-modal').delay(1500).fadeOut("slow").modal('hide');

             
            // $('#login-modal').delay(8000).modal('hide');
        });

    

        return false
    });
    
    $('#login_register_btn').click( function () { modalAnimate($formLogin, $formRegister) });
    $('#register_login_btn').click( function () { modalAnimate($formRegister, $formLogin); });
    $('#login_lost_btn').click( function () { modalAnimate($formLogin, $formLost); });
    $('#lost_login_btn').click( function () { modalAnimate($formLost, $formLogin); });
    $('#lost_register_btn').click( function () { modalAnimate($formLost, $formRegister); });
    $('#register_lost_btn').click( function () { modalAnimate($formRegister, $formLost); });
    
    function modalAnimate ($oldForm, $newForm) {
        var $oldH = $oldForm.height();
        var $newH = $newForm.height();
        $divForms.css("height",$oldH);
        $oldForm.fadeToggle($modalAnimateTime, function(){
            $divForms.animate({height: $newH}, $modalAnimateTime, function(){
                $newForm.fadeToggle($modalAnimateTime);
            });
        });
    }
    
    function msgFade ($msgId, $msgText) {
        $msgId.fadeOut($msgAnimateTime, function() {
            $(this).text($msgText).fadeIn($msgAnimateTime);
        });
    }
    
    function msgChange($divTag, $iconTag, $textTag, $divClass, $iconClass, $msgText) {
        var $msgOld = $divTag.text();
        msgFade($textTag, $msgText);
        $divTag.addClass($divClass);
        $iconTag.removeClass("glyphicon-chevron-right");
        $iconTag.addClass($iconClass + " " + $divClass);
        setTimeout(function() {
            msgFade($textTag, $msgOld);
            $divTag.removeClass($divClass);
            $iconTag.addClass("glyphicon-chevron-right");
            $iconTag.removeClass($iconClass + " " + $divClass);
  		}, $msgShowTime);
    }
});