
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" CONTENT="Caroline">

    <title>Integrated Radiological Image Search Engisne</title>
    <!-- Custom CSS -->
    <link href="static/css/heroic-features.css" rel="stylesheet">
    <link href="static/css/jasny-bootstrap.min.css" rel="stylesheet">
  
    <!-- Bootstrap Core CSS -->
    <link href="static/css/bootstrap.min.css" rel="stylesheet">
    <link href="static/css/lightslider.css" rel="stylesheet">
    <link href="static/css/mainresultstyles.css" rel="stylesheet">
    <link href="static/css/about.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <script src="static/js/jquery.js"></script>
    <script src="static/js/js.cookie.js"></script>
    <script src="static/js/html5shiv.min.js"></script>
    <script src="static/js/bootstrap.min.js"></script>
    <!--<script src="js/respond.min.js"></script>-->

    <script src="static/js/jasny-bootstrap.min.js"></script>
    <script src="static/js/lightslider.js"></script>
    <script src="static/js/jquery.elevatezoom.js"></script>
    <!-- comment for accuracy measurement only -->
    <script src="static/js/login.js"></script>
    <script src="static/js/common.js"></script>

    <script type="text/javascript">

        $(document).ready(function() {

        });
    </script>

</head>

<body>

% include('master_template.tpl')

<!-- Page Content -->
<div class="container">


    <div class="row">
        <div class="col-lg-12">
        </div>
    </div>
 
    <div class="row">

        <!-- The Search Block -->
        <div class="col-xs-10">
           <div class="panel panel-lightblue">
            <div class="panel-heading">Contact</div>
            <div class="panel-body">
<div>
    <p><strong>Lab Name: Medical Informatics Lab (MedIX)</strong></p>
    <p>Contact: <a href="http://facweb.cs.depaul.edu/research/vc/contact.html">http://facweb.cs.depaul.edu/research/vc/contact.html</a></p>
    <p>Website: <a href="http://facweb.cs.depaul.edu/research/vc/index.html">http://facweb.cs.depaul.edu/research/vc/index.html</a></p>
</div>



<div>
    <p><strong>Lab Name: Data Systems and Optimization Laboratory</strong></p>
    <p>Contact:312-362-7008</p>
    <p>Website: <a href="http://dbgroup.cdm.depaul.edu/">http://dbgroup.cdm.depaul.edu/</a></p>
</div>

<div>
    <p><strong>Lab Name: LIHCA</strong></p>
    <p>Contact: 312-362-7115</p>
    <p>Website: <a href="http://www.cdm.depaul.edu/about/pages/people/facultyinfo.aspx?fid=1311">http://www.cdm.depaul.edu/about/pages/people/facultyinfo.aspx?fid=1311</a></p>
</div>
            </div>
          </div>
        </div>

    </div>


<div class="clean"></div>
    <!-- Footer -->
    <footer>
        <div class="row">
            <div class="col-lg-12">
                <p>IRIS Search Engine is Copyright by DePaul 2017, all rights reserved</p>
            </div>
        </div>
    </footer>

</div>

<!-- /.container -->

<!-- jQuery -->
<!-- <script src="js/jquery.js"></script> -->

<!-- Bootstrap Core JavaScript -->
<!-- <script src="js/bootstrap.min.js"></script>
 -->
</body>

</html>
