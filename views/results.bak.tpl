
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" CONTENT="Caroline">

    <title>Integrated Radiological Image Search Engine</title>

    <!-- Bootstrap Core CSS -->
    <link href="static/css/bootstrap.min.css" rel="stylesheet">
    <link href="static/css/lightslider.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="static/css/heroic-features.css" rel="stylesheet">
    <link href="static/css/jasny-bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <script src="static/js/jquery.js"></script>
    <script src="static/js/html5shiv.min.js"></script>
    <script src="static/js/bootstrap.min.js"></script>
    <!--<script src="js/respond.min.js"></script>-->

    <script src="static/js/jasny-bootstrap.min.js"></script>
    <script src="static/js/lightslider.js"></script>
    <script src="static/js/jquery.elevatezoom.js"></script>
    <script src="static/js/login.js"></script>

    <script type="text/javascript">

        
        // $(document).ready(function() {
        //     $("#test1").lightSlider(); 
        //     $("#test2").lightSlider(); 
        // });
        function toggler(divId) {
          $("#" + divId).toggle();
        }        
    </script>



</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">IRIS</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
                    <a href="#">About</a>
                </li>
                <li>
                    <a href="#">Services</a>
                </li>
                <li>
                    <a href="#">Contact</a>
                </li>
            </ul>
               <ul class="nav navbar-nav navbar-right">
        <form class="navbar-form" role="search" action="/mainsearch" method="post">
        <div class="input-group">
            <input type="text" class="form-control" placeholder="Search" name="search_txt">
            <div class="input-group-btn">

                <button class="btn btn-default" type="submit">
                <i class="glyphicon glyphicon-search"></i></button>
                 <div class="btn-group" role="group">
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Advanced
                  <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu">
                  <li>
                  <!-- <a href="#"></a> -->
                  <a href="#" onclick="toggler('searchContent');">By Image</a>
                  </li>
                  </ul>
            </div>
            </div>
        </div>
        <a href="#" class="btn btn-primary btn-lg" role="button" data-toggle="modal" data-target="#login-modal">Log In</a>
  </div>         
        
       
        </form>
    </ul>

        </div>
             <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

<!-- Page Content -->
<div class="container">

    <!-- Jumbotron Header -->

<form id="searchContent" action="/search" method="post" style="display:none";>
        <div class="col-lg-12" style="background-color:lightgray;border-radius: 15px;">
            <h3>Search Parameters:</h3>
            <div class="input-group">
                <input type="text" class="form-control" name="search_txt" placeholder="Search for...">
                <span class="input-group-btn">
               <!--  <button class="btn btn-secondary" type="button">Go!</button> -->
                <input class="btn btn-secondary" value="go" type="submit" />
                </span>
                
            </div>
            <div class="fileinput fileinput-new" data-provides="fileinput">
                <p>{{searchresult_errorMsg}}</p>
                <p>Or search with uploading image:</p>

                <div class="fileinput-new thumbnail" style="width: 350px; height: 150px;">
                    <img data-src="holder.js/100%x100%" alt="...">
                </div>
                <div class="fileinput-preview fileinput-exists thumbnail"
                     style="max-width: 350px; max-height: 150px;"></div>
                <div>
                <span class="btn btn-default btn-file">
                    <span class="fileinput-new">Select image</span>
                    <span class="fileinput-exists">Change</span>
                    <input type="file" name="..."></span>
                    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                </div>
            </div>
            <p>
                <a class="btn btn-primary btn-large">Search with uploaded Image!</a>
            </p>

        </div>
</form>



    <hr>

    <!-- Title -->
    <div class="row">
        <div class="col-lg-12">
<!--             <h3>Best Match of Cardiomegaly</h3> -->
        </div>
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="col-md-7">
<!--             <a href="#">
                <img class="img-responsive"
                     src="https://www.mypacs.net/repos/mpv4_repo/viz/full/0/60/573/63501424.jpg"
                     alt="">
            </a> -->
        </div>
        <div class="col-md-5">
           <!--  <h3>Enlarged Heart</h3>
            <h4>Faculty and residents Children's Hospital, Radiologist, Children's Health System, Birmingham, Alabama., USA.</h4>
            <p>From MyPACS.net</p>
            <p>5 month old with failure to thrive. Abnormality seen on radiograph of the chest. </p>
            <a class="btn btn-primary" href="#">More Info <span class="glyphicon glyphicon-chevron-right"></span></a> -->
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
           <h4>{{searchresult_errorMsg}}</h4>
           <h3>Matching Results of {{search_text}}</h3>
        </div>
    </div>
 
%if defined('searchResult'):
%zoom_number=0
  % for item in searchResult:
        %zoom_number+=1
        <div class="col-md-3 col-sm-6 hero-feature">
            <div class="thumbnail">
                <!-- <img src="../static/img/FullSizeRender2.jpg"
                     alt=""> -->
                <img id="zoom_{{zoom_number}}" src="{{item['imagepath']}}" data-zoom-image="{{item['imagepath']}}"
                     alt="">

            </div>
            <script type="text/javascript">$("#zoom_{{zoom_number}}").elevateZoom({zoomWindowPosition:6});  </script>
            <div class="caption">
                <h3>Title: {{item['title']}}</h3>

                <p>Author: {{item['authors']}}</p>
                <p>Finding: {{item['findings']}}<p>
                <p>
                    <!--<a href="#" class="btn btn-primary">Buy Now!</a> -->
                    <a href="detailpage?id={{item['tfid']}}" class="btn btn-default">More
                        Info</a>
                    <a class="btn btn-default" href="http://localhost:8087/survey">Yes</a>
                    <a class="btn btn-default">No</a>
                    <a class="btn btn-default">Not Sure</a>
                </p>
            </div>
            <div class="clean"></div>
        </div>

  % end
%end

<div class="clean"></div>
<nav aria-label="Page navigation example" class="navigation">
  <ul class="pagination">
    <li class="page-item"><a class="page-link" href="#">Previous</a></li>
  %for i in range(0,totalpagenumber):
    %page_txt=str(i+1)
    %pageurl="http://localhost:8087/mainsearch?page="+page_txt+"&q="+search_text
    %if page_txt==current_page:
    <li class="page-item active"><a class="page-link" href="{{pageurl}}">{{i+1}}</a></li>
    %else:
    <li class="page-item"><a class="page-link" href="{{pageurl}}">{{i+1}}</a></li>
    %end
  % end 
    <li class="page-item"><a class="page-link" href="#">Next</a></li>
  </ul>
</nav>
  <!--   </div> -->
    <!-- /.row -->

    <hr>

    <!-- Footer -->
    <footer>
        <div class="row">
            <div class="col-lg-12">
                <p>Copyright &copy; DePaul 2017</p>
            </div>
        </div>
    </footer>

</div>

<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
      <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" align="center">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
          </button>
        </div>
                
                <!-- Begin # DIV Form -->
                <div id="div-forms">
                
                    <!-- Begin # Login Form -->
                    <form id="login-form">
                    <div class="modal-body">
                <div id="div-login-msg">
                                <div id="icon-login-msg" class="glyphicon glyphicon-chevron-right"></div>
                                <span id="text-login-msg">Type your username and password.</span>
                            </div>
                <input id="login_username" class="form-control" type="text" placeholder="E-Mail" required>
                <input id="login_password" class="form-control" type="password" placeholder="Password" required>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox"> Remember me
                                </label>
                            </div>
                  </div>
                <div class="modal-footer">
                            <div>
                                <button type="submit" class="btn btn-primary btn-lg btn-block">Login</button>
                            </div>
                  <div>
                                <button id="login_lost_btn" type="button" class="btn btn-link">Lost Password?</button>
                                <button id="login_register_btn" type="button" class="btn btn-link">Register</button>
                            </div>
                </div>
                    </form>
                    <!-- End # Login Form -->
                    
                    <!-- Begin | Lost Password Form -->
                    <form id="lost-form" style="display:none;">
                  <div class="modal-body">
                <div id="div-lost-msg">
                                <div id="icon-lost-msg" class="glyphicon glyphicon-chevron-right"></div>
                                <span id="text-lost-msg">Type your e-mail.</span>
                            </div>
                <input id="lost_email" class="form-control" type="text" placeholder="E-Mail" required>
                  </div>
                <div class="modal-footer">
                            <div>
                                <button type="submit" class="btn btn-primary btn-lg btn-block">Send</button>
                            </div>
                            <div>
                                <button id="lost_login_btn" type="button" class="btn btn-link">Log In</button>
                                <button id="lost_register_btn" type="button" class="btn btn-link">Register</button>
                            </div>
                </div>
                    </form>
                    <!-- End | Lost Password Form -->
                    
                    <!-- Begin | Register Form -->
                    <form id="register-form" style="display:none;" action="/register" method="post">
                    <div class="modal-body">
                            <div id="div-register-msg">
                                <div id="icon-register-msg" class="glyphicon glyphicon-chevron-right"></div>
                                <span id="text-register-msg">Register an account.</span>
                            </div>
                            <input id="register_FirstName" name="registerFirstName" class="form-control" type="name" placeholder="first name" required>
                            <input id="register_LastName" name="registerLastName" class="form-control" type="name" placeholder="last name" required>
                            <input id="register_Email" name="registerEmail" class="form-control" type="text" placeholder="E-Mail" required>
                            <input id="register_Password" name="registerPassword" class="form-control" type="password" placeholder="Password" required>
                            <div class="dropdown" id="register_Designation" name="registerDesignation" >
                              <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Choose Designation
                              </button>
                              <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                <button class="dropdown-item" type="button">Student</button>
                                <button class="dropdown-item" type="button">Radiologists</button>
                                <button class="dropdown-item" type="button">Professor</button>
                                <button class="dropdown-item" type="button">Other</button>
                              </div>
                            </div>
                  </div>
                <div class="modal-footer">
                            <div>
                                <button type="submit" class="btn btn-primary btn-lg btn-block">Register</button>
                            </div>
                            <div>
                                <button id="register_login_btn" type="button" class="btn btn-link">Log In</button>
                                <button id="register_lost_btn" type="button" class="btn btn-link">Lost Password?</button>
                            </div>
                </div>
            </form>
                    <!-- End | Register Form -->
                    
                </div>
                <!-- End # DIV Form -->
                
      </div>
    </div>
  </div>
<!-- /.container -->

<!-- jQuery -->
<!-- <script src="js/jquery.js"></script> -->

<!-- Bootstrap Core JavaScript -->
<!-- <script src="js/bootstrap.min.js"></script>
 -->
</body>

</html>
