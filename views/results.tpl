
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" CONTENT="Caroline">

    <title>Integrated Radiological Image Search Engisne</title>
    <!-- Custom CSS -->
    <link href="static/css/heroic-features.css" rel="stylesheet">
    <link href="static/css/jasny-bootstrap.min.css" rel="stylesheet">
  
    <!-- Bootstrap Core CSS -->
    <link href="static/css/bootstrap.min.css" rel="stylesheet">
    <link href="static/css/lightslider.css" rel="stylesheet">
    <link href="static/css/mainresultstyles.css" rel="stylesheet">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <script src="static/js/jquery.js"></script>
    <script src="static/js/js.cookie.js"></script>
    <script src="static/js/html5shiv.min.js"></script>
    <script src="static/js/bootstrap.min.js"></script>
    <!--<script src="js/respond.min.js"></script>-->

    <script src="static/js/jasny-bootstrap.min.js"></script>
    <script src="static/js/lightslider.js"></script>
    <script src="static/js/jquery.elevatezoom.js"></script>
    <!-- comment for accuracy measurement only -->
    <script src="static/js/login.js"></script>
    <script src="static/js/common.js"></script>
    <script src="static/js/dwv-0.22.0.min.js"></script>
    

    <script type="text/javascript">
        function submitFunction() {
           
            if ($("#s").val().length == 0 && !$("#image_exist").has("img")) {
                    alert("Please enter keywork or upload image for searching");
                    return false;
            }
            else if ($("#s").val().length == 0 && $("#image_exist").has("img")) {
                document.search_form.action="/imagesearch";
            }
            else if ($("#s").val().length != 0 && $("#image_exist").has("img"))
            {
                document.search_form.action="/bothsearch";
            }
            else{
                document.search_form.action="/mainsearch";
            }

            $.ajax({url: '/clearcookie',type: 'GET' });
            document.search_form.submit()
            return true;
        }
        function toggler(divId) {
          $("#" + divId).toggle();
        }

        function resetElevateZooms() {
            $("#zoom_bestMatch_0").elevateZoom({zoomWindowPosition:10});

        %if defined('topNMatches'):
            %zoom_number=0
            %for item in topNMatches:
                %zoom_number+=1
                //imageTopMatchesGallery_{{zoom_number}}
                $("#zoom_topMatch_{{zoom_number}}_0").elevateZoom({zoomWindowPosition:6});
            %end
        %end
        %if defined('alternative'):
            %zoom_number=0
            %for item in alternative:
                %zoom_number+=1
                $("#zoom_alternative_{{zoom_number}}_0").elevateZoom({zoomWindowPosition:6});
            %end
        %end


        }


        $(document).ready(function() {
            resetElevateZooms();
            $("#bestMatchImageGallery").lightSlider({
                gallery:true,
                // thumbItem:9,
                item:1,
                loop:true,
                slideMargin:0,
                enableDrag: false,
                currentPagerPosition: 'middle',
                enableTouch: false,
                enableDrag: false,
                controls: false,
                onBeforeSlide: function (el) {
                    $(".zoomContainer").remove()
                },
                onAfterSlide: function (el) {
                    $(el).find("li.active img").elevateZoom({zoomWindowPosition:10});
                }
            });

        });
    </script>

</head>

<body>

% include('master_template.tpl')

<!-- Page Content -->
<div class="container">

    <!-- Jumbotron Header -->

<!-- <form id="searchContent" action="/mainsearch" method="post" style="display:none">
        <div class="col-lg-12" style="background-color:lightgray;border-radius: 15px;">
            <h3>Search Parameters:</h3>
            <div class="input-group">
                <input type="text" class="form-control" name="search_txt" placeholder="Search for...">
                <span class="input-group-btn">
                <input class="btn btn-secondary" value="go" type="submit" />
                </span>
                
            </div>
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <p>{{searchresult_errorMsg}}</p>
                        <p style="color: lightgrey;">Or search by uploading image:</p>

                        <div class="fileinput-new thumbnail" style="width: 350px; height: 150px;">
                            <img data-src="./holder.js/100%x100%" alt="...">
                        </div>

                        <div class="fileinput-preview fileinput-exists thumbnail"
                             style="max-width: 350px; max-height: 350px;"></div>
                        <div>
                            <span class="btn btn-default btn-file">
                            <span class="fileinput-new">Select image</span>
                            <span class="fileinput-exists">Change</span>
                            <input type="file" name="imageupload"/></span>
                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                        </div>
                    </div>

            <p>
                <a class="btn btn-primary btn-large">Search with uploaded Image!</a>
            </p>

        </div>
</form> -->

    <div class="row">
        <div class="col-lg-12">
            <h4>{{searchresult_errorMsg}}</h4>
            %if defined('search_text'):
            <h3>Search results for: {{search_text}}</h3>
            %end
        </div>
    </div>
 
    <div class="row">

        <!-- The Search Block -->
        <div class="col-xs-3">
           <div class="panel panel-lightblue">
            <div class="panel-heading">Search Parameters</div>
            <div class="panel-body">
                <!-- <form id="searchForm" action="/mainsearch" method="post"> -->
                <form id="searchForm" action="/mainsearch" method="post" name="search_form" enctype="multipart/form-data">
                    <fieldset>
                    
                        <input id="s" class="form-control" type="text" name="search_txt"/>

                        <input type="submit" value="Search" class="btn btn-default btn-file" onClick="return submitFunction()"/>
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <p>{{searchresult_errorMsg}}</p>
                            <p style="color: grey;">Or search by uploading image:</p>

                            <div class="fileinput-new thumbnail">
                                <img data-src="/holder.js/100%x100%" alt="">
                            </div>
                            <div id="image_exist" class="fileinput-preview fileinput-exists thumbnail"></div>
                            <div>
                            <span class="btn btn-default btn-file">
                                <span class="fileinput-new">Select image</span>
                                <span class="fileinput-exists">Change</span>
                                <input id="image_upload_file" type="file" name="imageupload"/></span>
                                <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                            </div>
                        </div>

                    </fieldset>
                </form>

                <hr>

                <h5> Refine Search: </h5>
                <div class="refine col-md-5">

                <div class="checkbox" >
                <p>Section</p>
                    <label>
                        <input type="checkbox" disabled="disabled" >Thoracic
                    </label>
                    
                    <label>
                        <input type="checkbox" disabled="disabled" >Abdominal
                    </label>
                    
                <p>Modality</p>
                    <label>
                        <input type="checkbox" disabled="disabled" >CT
                    </label>
                    <label>
                        <input type="checkbox" disabled="disabled" >X-Ray
                    </label>
                    <label>
                        <input type="checkbox" disabled="disabled" > MRI
                    </label>
                    <label>
                        <input type="checkbox" disabled="disabled" > Pediatric Only
                    </label>
                <p>System</p>
                    <label>
                        <input type="checkbox" disabled="disabled" >Pulmonary
                    </label>
                    <label>
                        <input type="checkbox" disabled="disabled" >Cardiac
                    </label>
                    <label>
                        <input type="checkbox" disabled="disabled" >Renal
                    </label>
                    <label>
                        <input type="checkbox" disabled="disabled" >Endocrine
                    </label>
                    
                </div>

                </div>

            </div>
          </div>
        </div>


        <!-- The Best Match Block -->
        <div class="bestMatch col-xs-4">
           <div class="panel panel-blue">
            <div class="panel-heading">Best Match</div>
            <div class="panel-body">
%if defined('bestMatch'):
    <div class="col-md-12 col-sm-12 hero-feature">

        <ul id="bestMatchImageGallery">
            %bestMatch_image_index = 0
            %for item in bestMatch['imagepaths']:
                <li data-thumb="{{item}}" data-src="{{item}}">
                    % if item[-3:]=="dcm":
                        <div id="dwv_bestMatch_{{bestMatch_image_index}}">
                            <div class="layerContainer">
                                <canvas class="imageLayer"></canvas>
                            </div>
                        </div>
                        <script type="text/javascript">
                            dwv.gui.getElement = dwv.gui.base.getElement;
                            dwv.gui.displayProgress = function (percent) {};

                            // create the dwv app
                            var app_bestMatch_{{bestMatch_image_index}} = new dwv.App();
                            // initialise with the id of the container div
                            app_bestMatch_{{bestMatch_image_index}}.init({
                                "containerDivId": "dwv_bestMatch_{{bestMatch_image_index}}"
                            });
                            // load dicom data

                            app_bestMatch_{{bestMatch_image_index}}.loadURLs(["./getdcmimage?image_url={{item}}"]);
                        </script>
                    
                    %else:

                        <div class="thumbnail">
                            
                            <img id="zoom_bestMatch_{{bestMatch_image_index}}" src="{{item}}" data-zoom-image="{{item}}" alt="" />
                        </div>
                    %end
                </li>
                %bestMatch_image_index += 1
            %end
        </ul>



        <div class="caption">
            <h3>Title: {{bestMatch['title']}}</h3>
            <p><span class="bolder">Findings:</span> {{bestMatch['findings']}}<p>
            <p><a href="detailpage?id={{bestMatch['tfid']}}" class="btn btn-default">More
                    Info</a></p>
            %if defined('relevant_term'):
            <p><span class="bolder">Relevant Terms:</span>
                %for term in bestMatch['relevant_term'][0:len(bestMatch['relevant_term'])-1]:
                    <span>{{term}},</span>
                %end
                <span>{{bestMatch['relevant_term'][len(bestMatch['relevant_term'])-1]}}</span>
            </p>
            %end
            <p class="diagnosis"><strong>Diagnosis:</strong>
                %if 'diagnosis' in bestMatch:
                    {{bestMatch['diagnosis']}}
                %end
            </p>
            <p><span class="bolder">Source:</span> {{bestMatch['datasource']}}</p>

            <p>
                <div>
                    <span class="bolder">Relevance Feedback:</span>
                    <!-- <a class="btn btn-default" href="" onclick="Evaluation('1','{{bestMatch["tfid"]}}','{{bestMatch["title"]}}','{{search_text}}','feedbacksection_bestmatch','bestmatch'); return false;">Yes</a>
                    <a class="btn btn-default" href="" onclick="Evaluation('0','{{bestMatch["tfid"]}}','{{bestMatch["title"]}}','{{search_text}}','feedbacksection_bestmatch','bestmatch'); return false;">No</a>
                    <a class="btn btn-default" href="" onclick="Evaluation('999','{{bestMatch["tfid"]}}','{{bestMatch["title"]}}','{{search_text}}','feedbacksection_bestmatch','bestmatch'); return false;">Not Sure</a> -->

                    <a class="surveyAnswer" href="" onclick="return Evaluation('1','{{bestMatch["tfid"]}}','{{bestMatch["title"]}}','{{search_text}}','feedbacksection_bestmatch','bestmatch',this); return false;">
                        <button type="button" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span></button>
                    </a>
                    <a class="surveyAnswer" href="" onclick="return Evaluation('0','{{bestMatch["tfid"]}}','{{bestMatch["title"]}}','{{search_text}}','feedbacksection_bestmatch','bestmatch',this); return false;">
                        <button type="button" class="btn btn-default btn-sm"> <span class="glyphicon glyphicon-thumbs-down" aria-hidden="true"></span></button>
                    </a>
                    <a class="surveyAnswer" href="" onclick="return Evaluation('999','{{bestMatch["tfid"]}}','{{bestMatch["title"]}}','{{search_text}}','feedbacksection_bestmatch','bestmatch',this); return false;">
                        <button type="button" class="btn btn-default btn-sm" title="Not sure"> <span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></button>
                    </a>
                </div>
                <div id="feedbacksection_bestmatch"></div>
            </p>
        </div>
        <div class="clean"></div>
    </div>
%end
            </div>
          </div>
        </div>

        <!-- The Related Block -->
        <div id="otherMatchesBlock" class="col-xs-5">
           <div class="panel panel-lightblue">
            <div class="panel-heading">Other Matches</div>
            <div class="panel-body">
                <h4> Next Three Best Matches: </h4>
%if defined('topNMatches'):
%zoom_number=0
    %for item in topNMatches:
        %zoom_number+=1
        <div class="col-md-4 col-sm-4">
            <ul id="imageTopMatchesGallery_{{zoom_number}}">
                %topMatch_image_index = 0
                %for path in item['imagepaths']:
                    <li data-thumb="{{path}}" data-src="{{path}}">
                    % if path[-3:]=='dcm':
                        <div id="dwv_topmatch_{{zoom_number}}_{{topMatch_image_index}}">
                            <div class="layerContainer">
                                <canvas class="imageLayer"></canvas>
                            </div>
                        </div>
                        <script type="text/javascript">
                            dwv.gui.getElement = dwv.gui.base.getElement;
                            dwv.gui.displayProgress = function (percent) {};

                            // create the dwv app
                            var app_topmatch_{{zoom_number}}_{{topMatch_image_index}} = new dwv.App();
                            // initialise with the id of the container div
                            app_topmatch_{{zoom_number}}_{{topMatch_image_index}}.init({
                                "containerDivId": "dwv_topmatch_{{zoom_number}}_{{topMatch_image_index}}"
                            });
                            // load dicom data
                            app_topmatch_{{zoom_number}}_{{topMatch_image_index}}.loadURLs(["./getdcmimage?image_url={{path}}"]);
                        </script>

                    %else:
                        <div class="thumbnail">
                            <img id="zoom_topMatch_{{zoom_number}}_{{topMatch_image_index}}" src="{{path}}" data-zoom-image="{{path}}" alt="" />
                        </div>
                    %end
                        </li>
                    

                    %topMatch_image_index += 1
                %end
            </ul>
            <script type="text/javascript">
                $("#imageTopMatchesGallery_{{zoom_number}}").lightSlider({
                    item:1,
                    loop:true,
                    slideMargin:0,
                    enableDrag: false,
                    currentPagerPosition: 'middle',
                    enableTouch: false,
                    enableDrag: false,
                    controls: true,

                    prevHtml: '<div><</div>',
                    nextHtml: '<div>></div>',

                    onBeforeSlide: function (el) {
                        $(".zoomContainer").remove()
                    },
                    onAfterSlide: function (el) {
                        $(el).find("li.active img").elevateZoom({zoomWindowPosition:6});
                    }
                });
            </script>

            <p class="title"><a href="detailpage?id={{item['tfid']}}" > {{item['title']}} </a></p>
            %if 'relevant_term' in item:
            <p class="relevant"><strong>Relevant Terms:</strong>
            
                %for term in item['relevant_term'][0:len(item['relevant_term'])-1]:
                    <span>{{term}},</span>
                %end
                <span>{{item['relevant_term'][len(item['relevant_term'])-1]}}</span>

            </p>
            %end
            <p class="diagnosis"><strong>Diagnosis:</strong>
            %if 'diagnosis' in item:
                {{item['diagnosis']}}
            %end
            </p>

            <span class="relevance_feedback">Relevance Feedback:</span>
            <div class="caption">
            
            <a class='surveyAnswer' href="#" onclick="return Evaluation('1','{{item["tfid"]}}','{{item["title"]}}','{{search_text}}','otherMatch{{zoom_number}}','othermatch',this); return false;">
                <button type="button" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span></button>
            </a>
            <a class='surveyAnswer' href="#" onclick="return Evaluation('0','{{item["tfid"]}}','{{item["title"]}}','{{search_text}}','otherMatch{{zoom_number}}','othermatch',this); return false;"><button type="button" class="btn btn-default btn-sm"> <span class="glyphicon glyphicon-thumbs-down" aria-hidden="true"></span></button></a>
            <a class='surveyAnswer' href="#" onclick="return Evaluation('999','{{item["tfid"]}}','{{item["title"]}}','{{search_text}}','otherMatch{{zoom_number}}','othermatch',this); return false;"><button type="button" class="btn btn-default btn-sm" title="Not sure"> <span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></button></a>
            </div>
            <div class='otherMatch{{zoom_number}}'>
            </div>
            <div class="clean"></div>
        </div>
    %end
%end
                
                %if defined('moreResultIds'):
                    <div class='more_result_button'><a href="otherresults_with_ids?moreResultIds={{moreResultIds}}">more results</a></div>
                %else:
                    <div class='more_result_button'><a href="otherresults?search_text={{search_text}}">more results</a></div>
                %end
                <div class="clean"></div>
                </br>
                <h4> Alternative Three Best Matches: 
                <!-- <a href="#" onclick="return false;" title="best matches based on diagnosis and ddx"> -->
                    <span class="glyphicon glyphicon-info-sign" title="best matches based on diagnosis and ddx"></span>
                <!-- </a>  -->

                </h4>



                <div id="alternativeArea">
                  <!--   <h3 id="alternativeLoading"> Loading the Alternative Diagnosis or Considerations ...... </h3> -->
                  %if defined('alternative'):
                    %zoom_number=0
                        %for item in alternative:
                            %zoom_number+=1
                            <div class="col-md-4 col-sm-4 hero-feature">
                                <ul id="imageAlternativeGallery_{{zoom_number}}">
                                    %alternative_image_index = 0
                                    %for path in item['imagepaths']:
                                        
                                        <li data-thumb="{{path}}" data-src="{{path}}">
                                            %if path[-3:]=='dcm':
                                                <div id="dwv_alternative_{{zoom_number}}_{{alternative_image_index}}">
                                                    <div class="layerContainer">
                                                        <canvas class="imageLayer"></canvas>
                                                    </div>
                                                </div>
                                                <script type="text/javascript">
                                                    dwv.gui.getElement = dwv.gui.base.getElement;
                                                    dwv.gui.displayProgress = function (percent) {};

                                                    // create the dwv app
                                                    var app_alternative_{{zoom_number}}_{{alternative_image_index}} = new dwv.App();
                                                    // initialise with the id of the container div
                                                    app_alternative_{{zoom_number}}_{{alternative_image_index}}.init({
                                                        "containerDivId": "dwv_alternative_{{zoom_number}}_{{alternative_image_index}}"
                                                    });
                                                    // load dicom data
                                                    app_alternative_{{zoom_number}}_{{alternative_image_index}}.loadURLs(["./getdcmimage?image_url={{path}}"]);
                                                </script>
                                      
                                            %else:
                                                <div class="thumbnail">
                                                    <img id="zoom_alternative_{{zoom_number}}_{{alternative_image_index}}" src="{{path}}" data-zoom-image="{{path}}" alt="" />
                                                </div>
                                            %end
                                        </li>
                                        %alternative_image_index += 1
                                    %end
                                </ul>
                                <script type="text/javascript">
                                    $("#imageAlternativeGallery_{{zoom_number}}").lightSlider({
                                        item:1,
                                        loop:true,
                                        slideMargin:0,
                                        enableDrag: false,
                                        currentPagerPosition: 'middle',
                                        enableTouch: false,
                                        enableDrag: false,
                                        controls: true,

                                        prevHtml: '<div><</div>',
                                        nextHtml: '<div>></div>',
                                        
                                        onBeforeSlide: function (el) {
                                            $(".zoomContainer").remove()
                                        },
                                        onAfterSlide: function (el) {
                                            $(el).find("li.active img").elevateZoom({zoomWindowPosition:6});
                                        }
                                    });
                                </script>

                                <p class="title"><a href="detailpage?id={{item['tfid']}}" > {{item['title']}} </a></p>
                                <p class="relevant"><strong>Relevant Terms:</strong>
                                
                                %for term in item['relevant_term'][0:len(item['relevant_term'])-1]:
                                 <span>{{term}},</span>
                                %end
                                <span>{{item['relevant_term'][len(item['relevant_term'])-1]}}</span>
                                </p>

                                <p class="diagnosis"><strong>Diagnosis:</strong>
                                {{item['diagnosis']}}
                                </p>
                                <span class="relevance_feedback">Relevance Feedback:</span>
                                <div class="caption">
                                
                                <a class='surveyAnswer' href="" onclick="return Evaluation('1','{{item["tfid"]}}','{{item["title"]}}','{{search_text}}','alternative{{zoom_number}}','alternative',this); return false;">
                                    <button type="button" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span></button>
                                </a>
                                <a class='surveyAnswer' href="" onclick="return Evaluation('0','{{item["tfid"]}}','{{item["title"]}}','{{search_text}}','alternative{{zoom_number}}','alternative',this); return false;"><button type="button" class="btn btn-default btn-sm"> <span class="glyphicon glyphicon-thumbs-down" aria-hidden="true"></span></button></a>
                                <a class='surveyAnswer' href="" onclick="return Evaluation('999','{{item["tfid"]}}','{{item["title"]}}','{{search_text}}','alternative{{zoom_number}}','alternative',this); return false;"><button type="button" class="btn btn-default btn-sm" title="Not sure"> <span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></button></a>
                                </div>


                                <div id='alternative{{zoom_number}}'>
                                </div>
                                <div class="clean"></div>
                            </div>
                        %end
                    %end
                </div>

            </div>
          </div>
        </div>
    </div>


<div class="clean"></div>
    <!-- Footer -->
    <footer>
    <div class="row">
        <div class="panel panel-info">
      <div class="panel-heading">Search Trajectory: <button type="button" class="btn btn-default btn-sm" onclick="cleanCookies(); return false;">clear</button></div>
      <div id="history_cookies" class="panel-body">
%if defined('search_history'):
    %for item in search_history:
      {{item}} ->
      <!-- <span class="glyphicon glyphicon-arrow-right"></span> -->
    %end
%end  
    </div> 
    </div>
    </div>
        <div class="row">
            <div class="col-lg-12">
                <p>IRIS Search Engine is Copyright by DePaul 2017, all rights reserved</p>
            </div>
        </div>
    </footer>

</div>


<div class="modal fade" id="submit_alert" tabindex="-1" role="dialog" aria-labelledby="submitFormLabel" aria-hidden="true" style="display: none;">

    <div><span> Please enter ketwords for searching.</span></div>
</div>
<!-- /.container -->

<!-- jQuery -->
<!-- <script src="js/jquery.js"></script> -->

<!-- Bootstrap Core JavaScript -->
<!-- <script src="js/bootstrap.min.js"></script>
 -->
</body>

</html>
