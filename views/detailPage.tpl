<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" CONTENT="Caroline">
    
    <title>IRIS Search Detail</title>
    <!-- Custom CSS -->
    <link href="static/css/heroic-features.css" rel="stylesheet">
    <link href="static/css/jasny-bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap Core CSS -->
    <link href="static/css/bootstrap.min.css" rel="stylesheet">
    <link href="static/css/lightslider.css" rel="stylesheet">

    <link href="static/css/searchpagestyles.css" rel="stylesheet" type="text/css"  />
    <link href="static/css/detail.css" rel="stylesheet">



    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <script src="static/js/jquery.js"></script>
    <script src="static/js/html5shiv.min.js"></script>
    <script src="static/js/bootstrap.min.js"></script>
    <!--<script src="js/respond.min.js"></script>-->

    <script src="static/js/jasny-bootstrap.min.js"></script>
    <script src="static/js/lightslider.js"></script>
    <script src="static/js/dwv-0.22.0.min.js"></script>
    
    <script type="text/javascript">
        function toggler(divId) {
          $("#" + divId).toggle();
        }

        jQuery(document).ready(function($) {
 
                $('#myCarousel').carousel({
                    // interval: 5000
                    interval: false
                });
         
                //Handles the carousel thumbnails
                $('[id^=carousel-selector-]').click(function () {
                    var id_selector = $(this).attr("id");
                    try {
                        var id = /-(\d+)$/.exec(id_selector)[1];
                        console.log(id_selector, id);
                        $('#myCarousel').carousel(parseInt(id));
                    } catch (e) {
                        console.log('Regex failed!', e);
                    }
                });
                // When the carousel slides, auto update the text
                // $('#myCarousel').on('slid.bs.carousel', function (e) {
                //      var id = $('.item.active').data('slide-number');
                //     $('#carousel-text').html($('#slide-content-'+id).html());
                // });
        });
    </script>


</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">IRIS Search Engine</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
                    <a href="#">About</a>
                </li>
                <li>
                    <a href="#">Services</a>
                </li>
                <li>
                    <a href="#">Contact</a>
                </li>
            </ul>
             

        </div>
             <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

<!-- Page Content -->
<div class="container">

    <!-- Jumbotron Header -->

    <form id="searchForm" action="/mainsearch" method="post" style="display: none;">
        <fieldset>
            <div class="col-xs-8">
                <div class="col-xs-10">
                    <div>
                        <input id="s" class="form-control" type="text" name="search_txt"/>
        <!--              <input type="text" class="form-control" name="search_txt" placeholder="Search for..."> -->
                    </div>
        <!--             <div id="searchInContainer">
                        <input type="radio" name="check" value="site" id="searchSite" checked />
                        <label for="searchSite" id="siteNameLabel">Search</label>
                        
                        <input type="radio" name="check" value="web" id="searchWeb" />
                        <label for="searchWeb">Search The Web</label>
                    </div> -->
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <p>{{searchresult_errorMsg}}</p>
                        <p style="color: lightgrey;">Or search by uploading image:</p>

                        <div class="fileinput-new thumbnail" style="width: 350px; height: 150px;">
                            <img data-src="holder.js/100%x100%" alt="...">
                        </div>

                        <div class="fileinput-preview fileinput-exists thumbnail"
                             style="max-width: 350px; max-height: 350px;"></div>
                        <div>
                            <span class="btn btn-default btn-file">
                            <span class="fileinput-new">Select image</span>
                            <span class="fileinput-exists">Change</span>
                            <input type="file" name="..."></span>
                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                        </div>
                    </div>
                </div>
                <div class="home_search_buttons col-xs-2">
                    <input type="submit" value="By Keyword" class="btn btn-secondary searchBox" />
                    <input type="submit" value="By Image" class="btn btn-secondary searchBox" disabled="disabled" />
                    <input type="submit" value="Both" class="btn btn-secondary searchBoth searchBox" disabled="disabled" />
                </div>

            </div>
            <div class="refine col-xs-4">
                <h5> Refine Search: </h5>

                <div class="checkbox">
                <p>Section</p>
                    <label>
                        <input type="checkbox" disabled="disabled" >Thoracic
                    </label>
                    
                    <label>
                        <input type="checkbox" disabled="disabled" >Abdominal
                    </label>
                    
                <p>Modality</p>
                    <label>
                        <input type="checkbox" disabled="disabled" >CT
                    </label>
                    <label>
                        <input type="checkbox" disabled="disabled" >X-Ray
                    </label>
                    <label>
                        <input type="checkbox" disabled="disabled" > MRI
                    </label>
                    <label>
                        <input type="checkbox" disabled="disabled" > Pediatric Only
                    </label>
                <p>System</p>
                    <label>
                        <input type="checkbox" disabled="disabled" >Pulmonary
                    </label>
                    <label>
                        <input type="checkbox" disabled="disabled" >Cardiac
                    </label>
                    <label>
                        <input type="checkbox" disabled="disabled" >Renal
                    </label>
                    <label>
                        <input type="checkbox" disabled="disabled" >Endocrine
                    </label>
                    
                </div>

            </div>



        </fieldset>
    </form>


<h1>
%if defined('infos'):
    {{infos['title']}}
%end
 </h1>
  <div class="tabbable-panel">
    <div class="tabbable-line">
      <ul class="nav nav-tabs ">
        <li class="active">
          <a href="#tab_default_1" data-toggle="tab">
          Document </a>
        </li>
        <li>
          <a href="#tab_default_2" data-toggle="tab">
          History </a>
        </li>
        <li>
          <a href="#tab_default_3" data-toggle="tab">
          Findings </a>
        </li>
        <li>
          <a href="#tab_default_4" data-toggle="tab">
          Diagnosis </a>
        </li>
        <li>
          <a href="#tab_default_5" data-toggle="tab">
          DDx </a>
        </li>
          <li>
          <a href="#tab_default_6" data-toggle="tab">
          Discussion </a>
        </li>
          <li>
          <a href="#tab_default_7" data-toggle="tab">
          Comments </a>
        </li>
          <li>
          <a href="#tab_default_8" data-toggle="tab">
          References </a>
        </li>
      </ul>
    </div>
  </div>

 <div id="main_area">
        <!-- Slider -->
        <div class="row">
            <div class="col-sm-6" id="slider-thumbs">


                  <div class="tab-content">
                    <div class="tab-pane active" id="tab_default_1">
                      <p>
                        
                      </p>
                      <p>
                        MIRC
                      </p>
                    
                    </div>
                    <div class="tab-pane" id="tab_default_2">
        %if defined('infos'):
            {{infos['history']}}
        %end
                    
                    </div>
                    <div class="tab-pane" id="tab_default_3">
        %if defined('infos'):
            {{infos['findings']}}
        %end
                 
                    </div>
                    <div class="tab-pane" id="tab_default_4">
        %if defined('infos'):
            {{infos['diagnosis']}}
        %end
                 
                    </div>
                    <div class="tab-pane" id="tab_default_5">
        %if defined('infos'):
            {{infos['ddx']}}
        %end
                    </div>
                    <div class="tab-pane" id="tab_default_6">
        %if defined('infos'):
            {{infos['discussion']}}
        %end
                    </div>
                    <div class="tab-pane" id="tab_default_7">
                        Commend
                    </div>
                    <div class="tab-pane" id="tab_default_7">
                        References
                    </div>
                  </div>

                  <hr />
                <!-- Bottom switcher of slider -->
                <ul class="hide-bullets detail-list">
%if defined('images'):
    % image_index=0
    % for item in images:
                    <li class="col-sm-3">
                        <a class="thumbnail" id="carousel-selector-{{image_index}}">
                            <!-- <img src="http://mirc.luriechildrens.org/storage/ss12/docs/20170301183531723/160318779403081_icon.jpeg"> -->
                        % if item[-3:]=="dcm":
                            <div id="dwv_detail_{{image_index}}">
                                <div class="layerContainer">
                                    <canvas class="imageLayer"></canvas>
                                </div>
                            </div>
                            <script type="text/javascript">
                                dwv.gui.getElement = dwv.gui.base.getElement;
                                dwv.gui.displayProgress = function (percent) {};

                                // create the dwv app
                                var app_detail_{{image_index}} = new dwv.App();
                                // initialise with the id of the container div
                                app_detail_{{image_index}}.init({
                                    "containerDivId": "dwv_detail_{{image_index}}"
                                });
                                // load dicom data

                                app_detail_{{image_index}}.loadURLs(["./getdcmimage?image_url={{item}}"]);
                            </script>
                    
                         %else:
  
                                <img id="{{image_index}}" src="{{item}}" data-zoom-image="{{item}}" alt="" />
                        %end
                         </a>
                    </li>
                    % image_index+=1
    %end
%end

                </ul>
            </div>
            <div class="col-sm-6 mainImage">
                <div class="col-xs-12" id="slider">
                    <!-- Top part of the slider -->
                    <div class="row">
                        <div class="col-sm-12" id="carousel-bounding-box">
                            <div class="carousel slide" id="myCarousel">
                                <!-- Carousel items -->
                                <div class="carousel-inner">
%if defined('images'):
    % image_index=0
    % for item in images:
        % if image_index==0:
                    <div class="active item" data-slide-number="{{image_index}}">
                        % if item[-3:]=="dcm":
                            <div id="dwv_detailmain_{{image_index}}">
                                <div class="layerContainer">
                                    <canvas class="imageLayer"></canvas>
                                </div>
                            </div>
                            <script type="text/javascript">
                                dwv.gui.getElement = dwv.gui.base.getElement;
                                dwv.gui.displayProgress = function (percent) {};

                                // create the dwv app
                                var app_detailmain_{{image_index}} = new dwv.App();
                                // initialise with the id of the container div
                                app_detailmain_{{image_index}}.init({
                                    "containerDivId": "dwv_detailmain_{{image_index}}"
                                });
                                // load dicom data

                                app_detailmain_{{image_index}}.loadURLs(["./getdcmimage?image_url={{item}}"]);
                            </script>
                    
                         %else:
                        <img src="{{item}}">
                        %end
                    </div>
        % else:
                    <div class="item" data-slide-number="{{image_index}}">

                        % if item[-3:]=="dcm":
                            <div id="dwv_detailmain_{{image_index}}">
                                <div class="layerContainer">
                                    <canvas class="imageLayer"></canvas>
                                </div>
                            </div>
                            <script type="text/javascript">
                                dwv.gui.getElement = dwv.gui.base.getElement;
                                dwv.gui.displayProgress = function (percent) {};

                                // create the dwv app
                                var app_detailmain_{{image_index}} = new dwv.App();
                                // initialise with the id of the container div
                                app_detailmain_{{image_index}}.init({
                                    "containerDivId": "dwv_detail_{{image_index}}"
                                });
                                // load dicom data

                                app_detailmain_{{image_index}}.loadURLs(["./getdcmimage?image_url={{item}}"]);
                            </script>
                    
                         %else:
                        <img src="{{item}}">
                        %end
                    </div>
        % end
        % image_index+=1
    %end
%end
                              
                                </div>
                                <!-- Carousel nav -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/Slider-->
        </div>

    </div>

   
  <!--   </div> -->
    <!-- /.row -->

    <hr>

    <!-- Footer -->
    <footer>
        <div class="row">
            <div class="col-lg-12">
                <p>IRIS Search Engine is Copyright by DePaul 2017, all rights reserved</p>
            </div>
        </div>
    </footer>

</div>
<!-- /.container -->

<!-- jQuery -->
<!-- <script src="js/jquery.js"></script> -->

<!-- Bootstrap Core JavaScript -->
<!-- <script src="js/bootstrap.min.js"></script>
 -->
</body>

</html>
