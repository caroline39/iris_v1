<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="author" CONTENT="Caroline">
<title>Image Search</title>
    
    <!-- Bootstrap Core CSS -->
    <link href="static/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="static/css/lightslider.css" rel="stylesheet">
    <link href="static/css/heroic-features.css" rel="stylesheet">
    <link href="static/css/jasny-bootstrap.min.css" rel="stylesheet">
    <link href="static/css/searchpagestyles.css" rel="stylesheet" type="text/css"  />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <script src="static/js/jquery.js"></script>
    <script src="static/js/js.cookie.js"></script>
    <script src="static/js/html5shiv.min.js"></script>
    <script src="static/js/bootstrap.min.js"></script>
    <!--<script src="js/respond.min.js"></script>-->

    <script src="static/js/jasny-bootstrap.min.js"></script>
    <script src="static/js/lightslider.js"></script>
    <script src="static/js/login.js"></script>
    <script src="static/js/common.js"></script>

    <script type="text/javascript">
        // $(document).ready(function($) {
        //     $('#searchForm').submit(function() {
        //         if ($("#s").val().length == 0 ) {
        //             alert("Please enter keywork for searching");
        //             return false;
        //         } else {
        //             $.ajax({url: '/clearcookie',type: 'GET' });
        //             return true;
        //         }
        //     });
        // });
        function submitFunction(i) {
           if (i==1) {
                if ($("#s").val().length == 0 ) {
                    alert("Please enter keywork for searching");
                    return false;
                }
                document.search_form.action="/mainsearch";
            } else if (i==2) {
                document.search_form.action="/imagesearch";
            } else if (i==3) {
                document.search_form.action="/bothsearch";
            }

            $.ajax({url: '/clearcookie',type: 'GET' });
            document.search_form.submit()
            return true;
        }
    </script>

</head>

<body>
% include('master_template.tpl')

<div id="page">

    <h1 id="h1style">IRIS <small>(Integrated Radiological Image Search Engine)</small></h1>
    
    
    <form id="searchForm" action="/mainsearch" method="post" name="search_form" enctype="multipart/form-data">
        <fieldset>
            <div class="col-xs-8">
                <div class="col-xs-10">
                    <div>
                        <input id="s" class="form-control" type="text" name="search_txt"/>
        <!--              <input type="text" class="form-control" name="search_txt" placeholder="Search for..."> -->
                    </div>
        <!--             <div id="searchInContainer">
                        <input type="radio" name="check" value="site" id="searchSite" checked />
                        <label for="searchSite" id="siteNameLabel">Search</label>
                        
                        <input type="radio" name="check" value="web" id="searchWeb" />
                        <label for="searchWeb">Search The Web</label>
                    </div> -->
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <p>{{searchresult_errorMsg}}</p>
                        <p style="color: lightgrey;">Or search by uploading image:</p>

                        <div class="fileinput-new thumbnail" style="width: 350px; height: 150px;">
                            <img data-src="holder.js/100%x100%" alt="...">
                        </div>

                        <div class="fileinput-preview fileinput-exists thumbnail"
                             style="max-width: 350px; max-height: 350px;"></div>
                        <div>
                            <span class="btn btn-default btn-file">
                            <span class="fileinput-new">Select image</span>
                            <span class="fileinput-exists">Change</span>
                            <input type="file" name="imageupload"/></span>
                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                        </div>
                    </div>
                </div>
                <div class="home_search_buttons col-xs-2">
                    <input type="submit" value="By Keyword" class="btn btn-secondary searchBox" onClick="return submitFunction(1)"/>
                    <input type="submit" value="By Image" class="btn btn-secondary searchBox" onClick="return submitFunction(2)"/>
                    <input type="submit" value="Both" class="btn btn-secondary searchBoth searchBox" onClick="return submitFunction(3)"/>
                </div>

            </div>
            <div class="refine col-xs-4">
                <h5> Refine Search: </h5>

                <div class="checkbox">
                <p>Section</p>
                    <label>
                        <input type="checkbox" disabled="disabled" >Thoracic
                    </label>
                    
                    <label>
                        <input type="checkbox" disabled="disabled" >Abdominal
                    </label>
                    
                <p>Modality</p>
                    <label>
                        <input type="checkbox" disabled="disabled" >CT
                    </label>
                    <label>
                        <input type="checkbox" disabled="disabled" >X-Ray
                    </label>
                    <label>
                        <input type="checkbox" disabled="disabled" > MRI
                    </label>
                    <label>
                        <input type="checkbox" disabled="disabled" > Pediatric Only
                    </label>
                <p>System</p>
                    <label>
                        <input type="checkbox" disabled="disabled" >Pulmonary
                    </label>
                    <label>
                        <input type="checkbox" disabled="disabled" >Cardiac
                    </label>
                    <label>
                        <input type="checkbox" disabled="disabled" >Renal
                    </label>
                    <label>
                        <input type="checkbox" disabled="disabled" >Endocrine
                    </label>
                    
                </div>

            </div>



        </fieldset>
    </form>

    <div id="resultsDiv"></div>
    
</div>

<!-- It would be great if you leave the link back to the tutorial. Thanks! -->
<p class="credit"><img class="depaul_logo" src="/static/img/Depaul_U_Logo.png"></img></br>IRIS Search Engine is Copyright by DePaul 2017, all rights reserved</p>
    
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<!-- <script src="static/js/searchpagescript.js"></script>
 --></body>
</html>
